# Bitbucket Pipelines - Docker Build Container example #

See [this link](https://bitbucket.org/site/master/issues/12790/building-and-pushing-docker-images) for background information regarding pipelines and docker images.

This guide will show you how to build (using a build container), package and push docker images using a private docker API server from within a Bitbucket Pipeline.

### Goals of this example. ###

1. Use Bitbucket Pipelines to work with docker.
2. Use a Docker build container to produce a statically linked binary. The build container is 215MB and contains things that the runtime doesn't need.
3. Package that binary into the smallest runtime image possible. This example produces a runnable docker image that is 1.59MB.

We use a similar pipeline for building node projects. Any project that requires a build should have a separate docker build container. Projects that don't require a build container are much simpler, you can skip the Dockerfile.build and build the runtime container only.

### How do I get set up? ###

* Set up DNS. docker.yourhost.com with ports 5000 and your API port exposed.
* Set up your Docker API Engine server.
* Set up your Docker Client image for use in the pipeline
* Set up or use an existing Docker Registry
* Configure pipelines with ENV vars

Set the following variables in pipeline environment. You can hard-code them in your bitbucket-pipeline.yml but then you will be angering the https://12factor.net/config gods.
```bash
DOCKER_SERVER=docker.yourhost.com:5000
DOCKER_USERNAME=flarfy
DOCKER_PASSWORD=glorpify
DOCKER_EMAIL=not@val.id
```

### Files and what they are for ###

**hello.go** - The go file that prints "hello world. everything is awesome!"

**Dockerfile** - Used to build runtime docker image. Should be minimal to reduce image size.

**Dockerfile.build** - Used as the Docker Build Container. Since this is only used for build it can be bloated.

**build.sh** - This runs the build container and builds the runtime image. Can be ran locally or in the pipeline.


### Gotchas ###

* You cannot use -v when running docker images. This limitation means you have to do some extra gymnastics to get built artifacts off of the container.
* Your private registry (not API server) needs to have certs and run https for it to work properly.
* You cannot use pipeline env vars in the name: field for the image.

### Who do I talk to? ###

* Chris Dornsife <chris@applariat.com>
* Pouya Na <pouya@applariat.com>