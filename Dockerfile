FROM scratch
MAINTAINER Chris Dornsife <chris@applariat.com>
ADD target/bitbucket-pipelines-lab runme
ENTRYPOINT ["/runme"]