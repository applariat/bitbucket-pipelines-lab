### Docker Images ###

The docker-api-engine is required. This is your docker API engine server.

The docker-client is the base image used for pipelines, has docker command and connects to docker API engine server and registries.

The docker-registry is optional, it can be used to host your private images. You can use docker hub or whatever registry you wish.

The docker-builder-alpine-go Dockerfile can be found here: https://github.com/applariat/docker-builder-alpine-go