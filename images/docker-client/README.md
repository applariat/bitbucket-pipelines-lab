This is the base image used by the pipeline. It is how you can use the docker command line within pipelines.

This image should never reside on a public registry. It has certs built into it that will allow anyone running it access to your docker API server.

To generate the certs, see:
https://docs.docker.com/engine/security/https/

You can use this base image for all pipelines. 


