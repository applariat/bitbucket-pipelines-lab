Install docker and docker-compose on Ubuntu:
https://docs.docker.com/compose/install/

Set up docker with systemd:
https://docs.docker.com/engine/admin/systemd/

Secure the server with certs
https://docs.docker.com/engine/security/https/

Don't forgot to expose the ports.