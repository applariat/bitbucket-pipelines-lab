#!/usr/bin/env bash

# This build.sh is segregated from the pipeline so it can be used for local development.

# For most cases these are the only two variables you should have to change
PROJ=${BITBUCKET_REPO_SLUG:-bitbucket-pipelines-lab}
DOCKER_SERVER=${DOCKER_SERVER:-docker.applariat.io:5000}

# The first arg you can pass in a tag, useful for tagging images or for master leaving it blank.
# Default tag is "latest"
TAG=${1:-latest}

# Create a unique name for the container. We need an instance of the container
# so we can pluck the artifact off of it. The name must be unique to avoid collisions and
# so we don't have to iterate to find the container id.
HASH=`date +%Y-%m-%d.%H.%M.%S`
BUILD_NAME=${PROJ}-build-${HASH}

# the artifact will be placed in the target dir. This part isn't needed in the pipeline
# but it is useful if you use this build script locally.
rm -rf target
mkdir target

docker build -t ${BUILD_NAME} -f Dockerfile.build . || exit 1

# this will create a container. We need it to grab the files we want.
# This is important because we cannot mount volumes within the pipeline.
docker create --name ${BUILD_NAME} ${BUILD_NAME} /bin/true || exit 1

# Copy the artifact from the build server to the local file system
docker cp ${BUILD_NAME}:/target/$PROJ ./target/$PROJ || exit 1

# clean up the container and build image
docker rm ${BUILD_NAME} || exit 1
docker rmi -f ${BUILD_NAME} || exit 1

# Now that the artifact is local, build your runtime image
docker build -t ${DOCKER_SERVER}/${PROJ}:${TAG} . || exit 1

# clean up target
rm -rf target
